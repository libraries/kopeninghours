# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kopeninghours package.
# Paolo Zamponi <feus73@gmail.com>, 2020, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: kopeninghours\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:28+0000\n"
"PO-Revision-Date: 2021-03-01 08:36+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.2\n"

#: lib/display.cpp:28
#, kde-format
msgid "Open"
msgstr "Apri"

#: lib/display.cpp:28
#, kde-format
msgid "Open (%1)"
msgstr "Apri (%1)"

#: lib/display.cpp:30
#, kde-format
msgid "Closed"
msgstr "Chiuso"

#: lib/display.cpp:30
#, kde-format
msgid "Closed (%1)"
msgstr "Chiuso (%1)"

#: lib/display.cpp:50
#, kde-format
msgid "Open for one more minute"
msgid_plural "Open for %1 more minutes"
msgstr[0] "Aperto ancora un minuto"
msgstr[1] "Aperto ancora %1 minuti"

#: lib/display.cpp:51
#, kde-format
msgid "Open for one more minute (%2)"
msgid_plural "Open for %1 more minutes (%2)"
msgstr[0] "Aperto ancora un minuto (%2)"
msgstr[1] "Aperto ancora %1 minuti (%2)"

#: lib/display.cpp:54
#, kde-format
msgid "Currently closed, opens in one minute"
msgid_plural "Currently closed, opens in %1 minutes"
msgstr[0] "Attualmente chiuso, apre tra un minuto"
msgstr[1] "Attualmente chiuso, apre tra %1 minuti"

#: lib/display.cpp:55
#, kde-format
msgid "Currently closed (%2), opens in one minute"
msgid_plural "Currently closed (%2), opens in %1 minutes"
msgstr[0] "Attualmente chiuso (%2), apre tra un minuto"
msgstr[1] "Attualmente chiuso (%2), apre tra %1 minuti"

#: lib/display.cpp:68
#, kde-format
msgid "Open for one more hour"
msgid_plural "Open for %1 more hours"
msgstr[0] "Aperto ancora per un'ora"
msgstr[1] "Aperto ancora per %1 ore"

#: lib/display.cpp:69
#, kde-format
msgid "Open for one more hour (%2)"
msgid_plural "Open for %1 more hours (%2)"
msgstr[0] "Aperto ancora per un'ora (%2)"
msgstr[1] "Aperto ancora per %1 ore (%2)"

#: lib/display.cpp:72
#, kde-format
msgid "Currently closed, opens in one hour"
msgid_plural "Currently closed, opens in %1 hours"
msgstr[0] "Attualmente chiuso, apre tra un'ora"
msgstr[1] "Attualmente chiuso, apre tra %1 ore"

#: lib/display.cpp:73
#, kde-format
msgid "Currently closed (%2), opens in one hour"
msgid_plural "Currently closed (%2), opens in %1 hours"
msgstr[0] "Attualmente chiuso (%2), apre tra un'ora"
msgstr[1] "Attualmente chiuso (%2), apre tra %1 ore"

#: lib/display.cpp:86
#, kde-format
msgid "Open for one more day"
msgid_plural "Open for %1 more days"
msgstr[0] "Aperto ancora per un giorno"
msgstr[1] "Aperto ancora per %1 giorni"

#: lib/display.cpp:87
#, kde-format
msgid "Open for one more day (%2)"
msgid_plural "Open for %1 more days (%2)"
msgstr[0] "Aperto ancora per un giorno (%2)"
msgstr[1] "Aperto ancora per %1 giorni (%2)"

#: lib/display.cpp:90
#, kde-format
msgid "Currently closed, opens in one day"
msgid_plural "Currently closed, opens in %1 days"
msgstr[0] "Attualmente chiuso, apre tra un giorno"
msgstr[1] "Attualmente chiuso, apre tra %1 giorni"

#: lib/display.cpp:91
#, kde-format
msgid "Currently closed (%2), opens in one day"
msgid_plural "Currently closed (%2), opens in %1 days"
msgstr[0] "Attualmente chiuso (%2), apre tra un giorno"
msgstr[1] "Attualmente chiuso (%2), apre tra %1 giorni"

#: lib/display.cpp:102
#, kde-format
msgid "Currently open"
msgstr "Attualmente aperto"

#: lib/display.cpp:102
#, kde-format
msgid "Currently open (%1)"
msgstr "Attualmente aperto (%1)"

#: lib/display.cpp:104
#, kde-format
msgid "Currently closed"
msgstr "Attualmente chiuso"

#: lib/display.cpp:104
#, kde-format
msgid "Currently closed (%1)"
msgstr "Attualmente chiuso (%1)"
